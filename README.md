# JupyterLab GitLab

A JupyterLab extension for accessing GitLab repositories.

### What this extension is

When you install this extension, an additional filebrowser tab will be added
to the left area of JupyterLab. This filebrowser allows you to select GitLab
repositories belonging to GitLab users and/or groups, browse their repositories, 
and open the files in those
repositories. If those files are notebooks, you can run them just as you would
any other notebook. You can also attach a kernel to text files and run those.
Basically, you should be able to open any file in a repository that JupyterLab can handle.

### What this extension is not

This is not an extension that provides full GitLab access, such as 
saving files, making commits, forking repositories, etc.  Many of these features will 
be added in a subsequent release.

## Prerequisites

* JupyterLab 0.31
* A GitLab account for accessing non-public repositories

## Installation

### 1. Installing the labextension

To install the labextension locally, enter the following in your terminal:
```bash
jupyter labextension install .
```
To install the extension from the npm repository, enter the following in your tewrminal:
```
[sudo] jupyter labextension install @enki-portal/gitlab
```

### 2. Getting your private key credentials from GitLab

You will need to generate a private key on Gitlab to authorize access of non-public repositories.
1. Log into your GitLab account.
1. Go to your account settings.
1. Choose "Access Tokens" from the "User Settings" menu panel.
1. Provide a token name, expiration dtate (if desired) and check the "api" access seting.
1. Click the "Create personal access token" button.
1. The page will reload.  Record the generated access token at the top of the page. Remember this value and keep it secret.

It is important to note that the "private key" string is, as the name suggests, a secret.
*Do not* share this value online, as people may be able to use it to impersonate you on GitLab.

### 3. Using the extension on JupyterLab

After you click on the extension tab (left side of the JupyterLab browser window), fill in the "User Name" text box with your GitLab user name.  Fill in the "User Code" text box with your private key (see above), and optionally fill in the "Group Name" text box with a group of projects that you would like to access (e.g., "ENKI-portal").  Note that if you fill in the "Group Name" text box, its contents take precedent over your "User Name" in the reposiotory listing.

## Development

For a development install (requires npm version 4 or later), open a terminal window and execute the following in the repository directory:

```bash
npm install
npm run build
jupyter labextension install . --no-build
```
Open an additional window and launch JupyterLab in "watch" mode:
```
jupyter lab --watch
```

## Publish the package to npmjs.org

*npm* is both a JavaScript package manager and the de facto registry for JavaScript software. You can sign up for an account on the *npmjs.com* site or create an account from the command line by running
```
npm adduser
```
and entering values when prompted.

Next, open the project ```package.json``` file in your text editor. Prefix the name field value with ```@your-npm-username>/`` so that the entire field reads ```"name": "@your-npm-username/gitlab"``` where you’ve replaced the string ```your-npm-username``` with your real username.  

Review the homepage, repository, license, and other supported package.json fields while you have the file open. Then open the README.md file and adjust the command in the Installation section so that it includes the full, username-prefixed package name you just included in the package.json file. For example:
```
jupyter labextension install @enki-portal/gitlab
```
Now run the following command to publish your package:
```
npm publish --access=public
```
Check that your package appears on the npm website. You can either search for it from the homepage or visit https://www.npmjs.com/package/@enki-portal/gitlab directly. If it doesn’t appear, make sure you’ve updated the package name properly in the package.json and run the npm command correctly. Compare your work with the state of the reference project at the 06-prepare-to-publish tag for further debugging.